from channels.routing import ProtocolTypeRouter, URLRouter
import stock_grapher.routing
application = ProtocolTypeRouter({
    'websocket': URLRouter(
        stock_grapher.routing.websocket_urlpatterns
    ),
})