import os
import django
from channels.routing import get_default_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "stocks.settings")
django.setup()

os.environ.setdefault('SERVER_GATEWAY_INTERFACE', 'Asynchronous')
application = get_default_application()
