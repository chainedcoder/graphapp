from . import models
from . import serializers
from rest_framework import viewsets, permissions, filters

class AppleStocks(viewsets.ModelViewSet):

    queryset = models.Apple.objects.all()
    serializer_class = serializers.AppleSerializer

    filter_backends = (filters.SearchFilter,)
    search_fields = ('price', 'created')



