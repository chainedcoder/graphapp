from channels.generic.websocket import WebsocketConsumer
import time
import random

class AppleConsumer(WebsocketConsumer):

    def connect(self):
        self.username = "Anonymous"
        self.accept()

        while True:
            time.sleep(1)
            price = random.randint(80,120)

            self.send(text_data=str(price))
        else:
            self.send(text_data=price)

    def receive(self, data, text_data):
        if text_data.startswith("/name"):
            self.username = text_data[5:].strip()
            self.send(text_data="[set your username to %s]" % self.username)
        else:
            self.send(text_data=self.username + ": " + text_data)

    def disconnect(self, message):
        pass
