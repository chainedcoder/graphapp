from . import models

from rest_framework import serializers


class AppleSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Apple
        fields = '__all__'




