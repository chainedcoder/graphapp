# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django import forms
from .models import Apple

# Register your models here.
class AppleAdminForm(forms.ModelForm):
    class Meta:
        model = Apple
        fields = '__all__'


class AppleAdmin(admin.ModelAdmin):
    form = AppleAdminForm

    list_display = [f.name for f in Apple._meta.fields]

admin.site.register(Apple, AppleAdmin)