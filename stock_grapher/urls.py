from django.conf.urls import include, url
from rest_framework import routers
from . import api
from . import views

router = routers.DefaultRouter()
#api url for searching apple stocks
router.register(r'apple', api.AppleStocks)

urlpatterns = (
    # api for the rest api
    url(r'^api/', include(router.urls)),
)

urlpatterns += (
    url(r'^$', views.AppleStocks.as_view(), name='home'),
)
