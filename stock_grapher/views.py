# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import View
import random



class AppleStocks(View):
    context_object_name = 'prices'
    template_name = 'home.html'

    def get(self, request, *args, **kwargs):

        return render(request, self.template_name, {'prices':random.randint(1,101)})

