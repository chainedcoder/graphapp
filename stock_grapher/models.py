# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Apple(models.Model):
    price = models.CharField(max_length=255)
    created = models.DecimalField(max_digits=5, decimal_places=2)
